cd /home/ckennington/git/gruber

name=gruberlog
if [[ -e logs/$name.log ]] ; then
    i=0
    while [[ -e logs/$name-$i.log ]] ; do
        let i++
    done
    name=$name-$i
fi
heroku logs -n 1500 >> logs/"$name".log 2>&1


