@if "%DEBUG%" == "" @echo off
set JAVA_OPTS=%* 
@rem ##########################################################################
@rem
@rem  gruber startup script for Windows
@rem
@rem ##########################################################################

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set APP_HOME=%DIRNAME%..

@rem Add default JVM options here. You can also use JAVA_OPTS and GRUBER_OPTS to pass JVM options to this script.
set DEFAULT_JVM_OPTS=

@rem Find java.exe
if defined JAVA_HOME goto findJavaFromJavaHome

set JAVA_EXE=java.exe
%JAVA_EXE% -version >NUL 2>&1
if "%ERRORLEVEL%" == "0" goto init

echo.
echo ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:findJavaFromJavaHome
set JAVA_HOME=%JAVA_HOME:"=%
set JAVA_EXE=%JAVA_HOME%/bin/java.exe

if exist "%JAVA_EXE%" goto init

echo.
echo ERROR: JAVA_HOME is set to an invalid directory: %JAVA_HOME%
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:init
@rem Get command-line arguments, handling Windows variants

if not "%OS%" == "Windows_NT" goto win9xME_args

:win9xME_args
@rem Slurp the command line arguments.
set CMD_LINE_ARGS=
set _SKIP=2

:win9xME_args_slurp
if "x%~1" == "x" goto execute

set CMD_LINE_ARGS=%*

:execute
@rem Setup the command line

set CLASSPATH=%APP_HOME%\lib\gruber-0.2.jar;%APP_HOME%\lib\httpclient-4.3.6.jar;%APP_HOME%\lib\balloontip-1.2.4.1.jar;%APP_HOME%\lib\org.json-2.0.jar;%APP_HOME%\lib\jfreechart-1.0.19.jar;%APP_HOME%\lib\jung-graph-impl-2.0.1.jar;%APP_HOME%\lib\jung-visualization-2.0.1.jar;%APP_HOME%\lib\marytts-runtime-5.2-beta2.jar;%APP_HOME%\lib\maltparser-1.8.1.jar;%APP_HOME%\lib\stanford-corenlp-3.5.2.jar;%APP_HOME%\lib\exp4j-0.4.7.jar;%APP_HOME%\lib\jflac-1.3-.jar;%APP_HOME%\lib\javaFlacEncoder-0.3.1-.jar;%APP_HOME%\lib\inpro-work-.jar;%APP_HOME%\lib\sphinx4-core-5prealpha-SNAPSHOT-.jar;%APP_HOME%\lib\dsg-german-.jar;%APP_HOME%\lib\cmu-english-.jar;%APP_HOME%\lib\sqlite-.jar;%APP_HOME%\lib\rt.jar;%APP_HOME%\lib\oaa2-.jar;%APP_HOME%\lib\jrtp_1.0a-.jar;%APP_HOME%\lib\antlr-oaa-.jar;%APP_HOME%\lib\jgraphx-.jar;%APP_HOME%\lib\instantreality-.jar;%APP_HOME%\lib\jsapi-.jar;%APP_HOME%\lib\voice-bits1-hsmm-5.1-.jar;%APP_HOME%\lib\drej-.jar;%APP_HOME%\lib\broker-.jar;%APP_HOME%\lib\trident-.jar;%APP_HOME%\lib\rsb-0.13.0-.jar;%APP_HOME%\lib\rst-0.10.1-.jar;%APP_HOME%\lib\protobuf-java-2.6.1-.jar;%APP_HOME%\lib\Cocolab_DE_8gau_13dCep_16k_40mel_130Hz_6800Hz-.jar;%APP_HOME%\lib\WSJ_8gau_13dCep_16k_40mel_130Hz_6800Hz-.jar;%APP_HOME%\lib\jaxb-impl-2.1.jar;%APP_HOME%\lib\log4j-1.2.17.jar;%APP_HOME%\lib\opennlp-tools-1.5.3.jar;%APP_HOME%\lib\opennlp-maxent-3.0.3.jar;%APP_HOME%\lib\jdom-1.1.3.jar;%APP_HOME%\lib\kaldi-client-1.0.jar;%APP_HOME%\lib\venice.lib-0.5.jar;%APP_HOME%\lib\marytts-client-5.2-SNAPSHOT.jar;%APP_HOME%\lib\marytts-lang-en-5.2-SNAPSHOT.jar;%APP_HOME%\lib\marytts-lang-de-5.2-SNAPSHOT.jar;%APP_HOME%\lib\xmlrpc-server-3.1.3.jar;%APP_HOME%\lib\xmlrpc-common-3.1.3.jar;%APP_HOME%\lib\xmlrpc-client-3.1.3.jar;%APP_HOME%\lib\commons-logging-1.2.jar;%APP_HOME%\lib\ws-commons-util-1.0.2.jar;%APP_HOME%\lib\weka-stable-3.6.14.jar;%APP_HOME%\lib\json-20151123.jar;%APP_HOME%\lib\backport-util-concurrent-1.1_01.jar;%APP_HOME%\lib\nbayes-.jar;%APP_HOME%\lib\wac-0.2.jar;%APP_HOME%\lib\inprotk-1.38.jar;%APP_HOME%\lib\sium-inpro-1.2.jar;%APP_HOME%\lib\dm-0.33.jar;%APP_HOME%\lib\opendial-1.43.jar;%APP_HOME%\lib\slf4j-api-1.7.12.jar;%APP_HOME%\lib\nextrtc-signaling-server-0.0.1.jar;%APP_HOME%\lib\jetty-server-9.3.8.v20160314.jar;%APP_HOME%\lib\jetty-servlet-9.3.8.v20160314.jar;%APP_HOME%\lib\jetty-webapp-9.3.8.v20160314.jar;%APP_HOME%\lib\jetty-util-9.3.8.v20160314.jar;%APP_HOME%\lib\websocket-server-9.3.8.v20160314.jar;%APP_HOME%\lib\javax-websocket-server-impl-9.3.8.v20160314.jar;%APP_HOME%\lib\websocket-client-9.3.8.v20160314.jar;%APP_HOME%\lib\websocket-servlet-9.3.8.v20160314.jar;%APP_HOME%\lib\websocket-api-9.3.8.v20160314.jar;%APP_HOME%\lib\javax-websocket-client-impl-9.3.8.v20160314.jar;%APP_HOME%\lib\websocket-common-9.3.8.v20160314.jar;%APP_HOME%\lib\spring-web-4.2.5.RELEASE.jar;%APP_HOME%\lib\spring-core-4.2.5.RELEASE.jar;%APP_HOME%\lib\spring-context-4.2.5.RELEASE.jar;%APP_HOME%\lib\spring-test-4.2.5.RELEASE.jar;%APP_HOME%\lib\json-simple-1.1.jar;%APP_HOME%\lib\httpcore-4.3.3.jar;%APP_HOME%\lib\commons-codec-1.6.jar;%APP_HOME%\lib\jcommon-1.0.23.jar;%APP_HOME%\lib\jung-api-2.0.1.jar;%APP_HOME%\lib\collections-generic-4.01.jar;%APP_HOME%\lib\jung-algorithms-2.0.1.jar;%APP_HOME%\lib\libsvm-3.1.jar;%APP_HOME%\lib\liblinear-1.8.jar;%APP_HOME%\lib\xom-1.2.10.jar;%APP_HOME%\lib\jollyday-0.4.7.jar;%APP_HOME%\lib\ejml-0.23.jar;%APP_HOME%\lib\javax.json-api-1.0.jar;%APP_HOME%\lib\jwnl-1.3.3.jar;%APP_HOME%\lib\java-diff-1.1.jar;%APP_HOME%\lib\java-cup-0.11a.jar;%APP_HOME%\lib\spring-websocket-4.1.4.RELEASE.jar;%APP_HOME%\lib\guava-18.0.jar;%APP_HOME%\lib\gson-2.2.4.jar;%APP_HOME%\lib\commons-lang3-3.3.2.jar;%APP_HOME%\lib\javax.servlet-api-3.1.0.jar;%APP_HOME%\lib\jetty-http-9.3.8.v20160314.jar;%APP_HOME%\lib\jetty-io-9.3.8.v20160314.jar;%APP_HOME%\lib\jetty-security-9.3.8.v20160314.jar;%APP_HOME%\lib\jetty-xml-9.3.8.v20160314.jar;%APP_HOME%\lib\jetty-annotations-9.3.8.v20160314.jar;%APP_HOME%\lib\javax.websocket-api-1.0.jar;%APP_HOME%\lib\spring-aop-4.2.5.RELEASE.jar;%APP_HOME%\lib\spring-beans-4.2.5.RELEASE.jar;%APP_HOME%\lib\colt-1.2.0.jar;%APP_HOME%\lib\xercesImpl-2.8.0.jar;%APP_HOME%\lib\xalan-2.7.0.jar;%APP_HOME%\lib\jaxb-api-2.2.7.jar;%APP_HOME%\lib\commons-lang-2.6.jar;%APP_HOME%\lib\commons-io-2.5.jar;%APP_HOME%\lib\fast-md5-2.7.1.jar;%APP_HOME%\lib\groovy-all-2.4.6.jar;%APP_HOME%\lib\Jampack-1.0.jar;%APP_HOME%\lib\jama-1.0.3.jar;%APP_HOME%\lib\swing-layout-1.0.3.jar;%APP_HOME%\lib\jetty-plus-9.3.8.v20160314.jar;%APP_HOME%\lib\javax.annotation-api-1.2.jar;%APP_HOME%\lib\asm-5.0.1.jar;%APP_HOME%\lib\asm-commons-5.0.1.jar;%APP_HOME%\lib\concurrent-1.3.4.jar;%APP_HOME%\lib\jetty-jndi-9.3.8.v20160314.jar;%APP_HOME%\lib\asm-tree-5.0.1.jar;%APP_HOME%\lib\icu4j-54.1.1.jar;%APP_HOME%\lib\commons-collections-3.2.1.jar;%APP_HOME%\lib\emotionml-checker-java-1.1.jar;%APP_HOME%\lib\jtok-core-1.9.3.jar;%APP_HOME%\lib\trove4j-2.0.2.jar;%APP_HOME%\lib\httpcore-nio-4.1.jar;%APP_HOME%\lib\hsqldb-2.0.0.jar;%APP_HOME%\lib\slf4j-log4j12-1.6.1.jar;%APP_HOME%\lib\commons-math3-3.6.1.jar;%APP_HOME%\lib\sium-0.74.jar;%APP_HOME%\lib\spring-expression-4.2.5.RELEASE.jar;%APP_HOME%\lib\aopalliance-1.0.jar;%APP_HOME%\lib\joda-time-2.8.2.jar;%APP_HOME%\lib\junit-4.11.jar;%APP_HOME%\lib\hamcrest-core-1.3.jar;%APP_HOME%\lib\xml-apis-2.0.2.jar;%APP_HOME%\lib\marytts-common-5.2-beta2.jar;%APP_HOME%\lib\marytts-signalproc-5.2-beta2.jar

@rem Execute gruber
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% %GRUBER_OPTS%  -classpath "%CLASSPATH%"  %CMD_LINE_ARGS%

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
rem Set variable GRUBER_EXIT_CONSOLE if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
if  not "" == "%GRUBER_EXIT_CONSOLE%" exit 1
exit /b 1

:mainEnd
if "%OS%"=="Windows_NT" endlocal

:omega
