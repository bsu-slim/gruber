\documentclass{sigchi}
% \usepackage{acl2016}
%\usepackage{geometry}

% \CopyrightYear{2016}
%\setcopyright{acmcopyright}
% \setcopyright{acmlicensed}

\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage[scaled=.92]{helvet} % for proper fonts
\usepackage{graphicx} % for EPS use the graphics package instead
\usepackage{balance}  % for useful for balancing the last columns
\usepackage{booktabs} % for pretty table rules
\usepackage{ccicons}  % for Creative Commons citation icons
\usepackage{ragged2e} % for tighter hyphenation


%\usepackage[scaled=.92]{helvet} % for proper fonts
%\usepackage{times}
% \usepackage[round]{natbib}
\usepackage{latexsym}
\usepackage[utf8]{inputenc}
% \usepackage[font=small,labelfont=bf]{caption}
\usepackage{amsmath}
\usepackage{multirow}
\usepackage{appendix}
\usepackage{url}
\usepackage{tikz}
% \usepackage{avm}
% \avmfont{\sc}
% \avmoptions{sorted}
% \avmvalfont{\rm}
% \avmsortfont{\scriptsize\it}
\usepackage{remreset}
\usepackage{pifont}
% \newcommand{\cmark}{\ding{53}}%
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{verbatim} 
\usepackage{linguex}
\usepackage{qtree}

\def\pprw{8.5in}
\def\pprh{11in}
\special{papersize=\pprw,\pprh}
\setlength{\paperwidth}{\pprw}
\setlength{\paperheight}{\pprh}
\setlength{\pdfpagewidth}{\pprw}
\setlength{\pdfpageheight}{\pprh}

%\usepackage{algorithm}% http://ctan.org/pkg/algorithms
%\usepackage{algpseudocode}% http://ctan.org/pkg/algorithmicx
\usepackage{amsmath,amsfonts,amssymb}

\newcommand{\argmax}{\operatornamewithlimits{argmax}}

\usepackage{color}
\newcommand{\das}[1]{{\color{red}\emph{//das: #1//}}}
\newcommand{\crk}[1]{\emph{//crk: #1//}}
\newcommand{\todo}[1]{\textcolor{green}{\emph{//todo: #1//}}}


\newcommand{\sds}[0]{\textsc{sds}}
\newcommand{\nlu}[0]{\textsc{nlu}}
\newcommand{\rmrs}[0]{\textsc{rmrs}}
\newcommand{\ep}[0]{\texttt{EP}}
\newcommand{\inprotk}[0]{\textsc{InproTK}}
\newcommand{\sium}[0]{\textsc{sium}}
\newcommand{\asr}[0]{\textsc{asr}}
\newcommand{\dm}[0]{\textsc{dm}}
\newcommand{\ui}[0]{\textsc{gui}}
\newcommand{\iu}[0]{\textsc{iu}}
\newcommand{\rr}[0]{\textsc{rr}}
\newcommand{\pa}[0]{\textsc{pa}}
\newcommand{\gui}[0]{\textsc{gui}}

\newcommand{\redcircle}{\tikz\draw[red,fill=red](0,0)circle(.6ex);}
\newcommand{\bluecircle}{\tikz\draw[blue](0,0)circle(.6ex);}

%Document Head
% \dochead{CLV2 Class File Manual}

%\title{An Adaptive, Incremental Personal Assistant that Graphically Signals Speech Understanding}
\title{A Graphical Digital Personal Assistant\\ that Grounds and Learns Autonomously}

{
\numberofauthors{3}
% Notice how author names are alternately typesetted to appear ordered
% in 2-column format; i.e., the first 4 autors on the first column and
% the other 4 auhors on the second column. Actually, it's up to you to
% strictly adhere to this author notation.
 \author{%
   \alignauthor{%
     Casey Kennington\\
     \affaddr{Boise State University} \\
     \affaddr{1910 University Dr.}
     \\
     \email{casykennington@boisestate.edu}} \alignauthor{%
     Aprajita Shukla\\
     \affaddr{Boise State University} \\
     \affaddr{1910 University Dr.} \\
     \email{aprajitashukla@boisestate.edu}}
    }
 }
    
\toappear{\scriptsize Permission to make digital or hard copies of all or part of this work for personal or classroom use is granted without fee provided that copies are not made or distributed for profit or commercial advantage and that copies bear this notice and the full citation on the first page. Copyrights for components of this work owned by others than ACM must be honored. Abstracting with credit is permitted. To copy otherwise, or republish, to post on servers or to redistribute to lists, requires prior specific permission and/or a fee. Request permissions from permissions@acm.org. \\
{\emph{HAI 2017, October 17--20, 2017, Bielefeld, Germany..} } \\
Copyright \copyright~2017 Association of Computing Machinery. \\
ACM ISBN 978-1-4503-5113-3/17/10\ ...\$15.00. \\https://doi.org/10.1145/3125739.3132592
}
% Update the XXXX string to your assigned DOI from ACM. 

\clubpenalty=10000 
\widowpenalty = 10000
  
\begin{document}
% \affil{Publishing / SPi}



\maketitle


\keywords{Grounding; Interactive Dialogue; Personal Assistant}

\category{H.5.2}{User Interfaces}{Information Systems}
%(e.g.,
%  HCI)}{Miscellaneous}\category{See}{\url{http://acm.org/about/class/1998/}}{for
%  full list of ACM classifiers. This section is required.}

% abstract should be 150-250 words
\begin{abstract}
We present a speech-driven digital personal assistant that is robust despite little or no training data and autonomously improves as it interacts with users. The system is able to establish and build common ground between itself and users by signaling understanding and by learning a mapping via interaction between the words that users actually speak and the system actions. We evaluated our system with real users and found an overall positive response. We further show through objective measures that autonomous learning improves performance in a simple itinerary filling task. 
\end{abstract}

\section{Introduction}
\label{section:intro}

In spoken interaction, participants signal understanding (e.g., by uttering backchannels) which shapes interaction by allowing conversation participants to know that their interlocutor is understanding what is being said. However, signaling understanding is a challenge for speech-driven agents \cite{Dethlefs2015}: some systems display the recognized transcript or utter \emph{okay} after a request has completed, but there is no guarantee that the request was actually understood and could lead to the wrong system action. Moreover, though many systems are based on data-driven robust statistical models, they are generally static in that the models have a predefined ontology and do not continue to improve as they interact with users. Taken together, these system shortcomings are due to a lack of \emph{conversational grounding} which is defined as building mutual understanding between dialogue participants \cite{clarkschaefer:contrdis}. Our goal is to improve system grounding by signaling backchannels to users in an intuitive way and by autonomously improving the mapping between what users say and system actions.

%though some current \pa s often display the automatic speech recognition (\asr) transcript of what a user is saying, it is not the case that correctly transcribed speech amounts to complete understanding and therefore cannot be trusted as a signal of understanding. Beyond displaying the transcript, \pa\ systems either only signal understanding after an entire user utterance is complete (e.g., by saying \emph{okay} followed by some kind of confirmation) or they simply perform the requested action--which could be the wrong action, potentially requiring the user to start her utterance from scratch. Moreover, though many systems are based on data-driven robust statistical models, they are generally static in that the models have a predefined ontology and do not continue to improve as they interact with users. Taken together, these system shorcomings are due to a lack of \emph{conversational grounding} which is defined as establishing and building common ground (i.e., mutual understanding) between dialogue participants process \cite{clarkschaefer:contrdis}. Our goal is to improve system grounding by signaling backchannels to users in an intuitive way and by improving the mapping between what users say and what action the system performs.

%In this paper, we explore how using a graphical user interface (\gui) can be leveraged for a \pa\ to improve converstational grounding by signaling system understanding and facilitating autonomous improvement of language understanding through interaction. 

Our personal assistant (\pa) system, which we explain further in Section~\ref{section:system_def}, works incrementally (i.e., it updates its internal state word by word) as it comprehends and gives visual cues of understanding through the \gui, and, crucially, if the system displays incorrect cues, a user can correct the misunderstanding immediately instead of at the end of the request. Because incremental processing lends itself to a system that can signal backchannels, an incremental system can also be more autonomous--requests that have been repaired and confirmed locally can be used as examples on how the system can improve understanding through conversational grounding. 

%Many digital \pa s are arguably accessed using mobile devices that have some kind of screen which allows a \gui\ to be a potentially effective interface to signal understanding and act as a facilitator for autonomous improvement.\footnote{Amazon recently announced the release of \emph{Echo Show} which, in line with our work, adds visual display capabilities to the known \emph{Alexa} digital \pa.} 

% To recap, our main contriubtution is to improve conversational grounding between a \pa\ and human users by:
% 
% \begin{itemize}
%  \item incremental \gui-driven backchannels which signal ongoing system understanding to the user
%  \item \gui-driven autonomous improvement of natural language understanding starting with little or no training data
% \end{itemize}

In Section~\ref{section:experiments} we explain how we evaluate our system with real users under two different settings: a baseline system and a system that learns autonomously. Our user evaluations show that our system is perceived as intuitive and useful, and we show through objective measures that it can autonomously improve through the interactive process. In the following section, we explain how we build off of previous work.

% \todo{play up auto discovery? learning from scratch?}
% \todo{so-called ``cold start''}

\section{Background and Related Work}
\label{section:related_work}

Though grounding between systems and users is a challenge \cite{Kruijff2012}, we build directly off of recent work that was perceived by users as natural and allowed users to accomplish many tasks in a short amount of time \cite{kennington-schlangen2016} and \cite{liu-fang-chai:2012:SIGDIAL2012,Chai2014} which addressed misalignments in understanding in a robot-human interaction scenarios. Also directly related is \cite{Hough2017a} which used a robot that could signal incremental understanding by performing actions (e.g., moving towards a referred object). Backchannels play a role in the grounding process; \cite{meena-skantze-gustafson:2013:SIGDIAL2}, for example, used prosodic and contextual features in order to produce a backchannel without overlapping with users' speech. We use a \gui\ to display backchannels (i.e., we need not worry about overlap with user speech). 

%We also go beyond this previous work in that our system autonomously improves as it interacts and the \gui\ we display is more informative (explained in the following section). This work is part of a long-term effort to build dialogue systems that are more personalized towards their users for long-term use, as argued for by \cite{Schlangen2012}.

\section{System Description}
\label{section:system_def}

%An aspect of our \pa\ that sets it apart from many other \pa s is the requirement that it process \emph{incrementally} which, we argue, is necessary for effective grounding. In this section, we motivate and explain what we mean by \emph{incremental dialogue processing}, then explain our system. 

% \begin{figure}[h]
% \centering
%     \includegraphics[width=0.8\columnwidth]{figures/iuexample.pdf}
%     \caption{\footnotesize Example of incremental speech recognition: the word \emph{four} is added then revoked, then replaced with \emph{forty}. The diamonds denote the point in time when the \iu\ is passed to the next module.}
%     \label{fig:iuexample}
% \end{figure}


\paragraph{Incremental Processing} Our system is built as a network of interconnected modules as proposed by the \emph{incremental unit} (\iu) framework \cite{Schlangen2011}, a theoretical framework for incremental dialogue processing where bits of information are encoded as the payload of \iu s; each module performs some kind of operation on those \iu s and produces \iu s of its own (e.g., a speech recognizer takes audio input and produces transcribed words as incremental units). The \iu\ framework allows us to design and build a personal assistant that can perform actions without delay which is crucial in building systems that can ground with the user by signaling ongoing understanding--an important prerequisite to autonomous learning (explained further below).

%One potential concern with incremental processing is regarding informativeness: why act early when waiting might provide additional information, resulting in better-informed system decisions? The trade off is \emph{naturalness} as perceived by the user who is interacting with the system. 

%Indeed, 
It has been shown that human users perceive incremental systems as being more natural than traditional, turn-based systems \cite{Aist2006,Skantze2009,skantze2010sigdial,Asri2014,kennington-schlangen2016}, offer a more human-like experience  \cite{Edlund2008b} and are more satisfying to interact with than non-incremental systems \cite{Aistetal:incrunder-short}. Moreover, psycholinguistic research has also shown that humans comprehend utterances as they unfold \cite{Tanenhaus1995,Spivey_2002tw}. 

\paragraph{System Overview} Our system builds directly off of \cite{kennington-schlangen2016}, which introduced a system composed of four main components: automatic speech recognition (\asr) which incrementally transcribes spoken utterances, natural language understanding (\nlu) explained below, a dialogue manager (\dm) using OpenDial \cite{Lison2015a} which determined when the system should \texttt{select} (i.e., fill a slot in a semantic frame), \texttt{wait} for more information, \texttt{request} confirmation, or \texttt{confirm} a confirmation request, and the final component was a \gui, also explained below. Figure~\ref{fig:sig16} conceptually shows these components and how the information (i.e., \iu s) flows between them. As our work focuses on improvements made to the \nlu\ and \gui\ to improve conversational grounding, we explain these two components in greater detail. 

\begin{figure}[h]
  \centering
      \includegraphics[width=0.4\textwidth]{figures/sds_gui.pdf}	
      \caption{System overview. \label{fig:sig16}}
\end{figure}

\begin{figure*}[ht]
  \centering
  {
    \setlength{\fboxsep}{0pt}%
\setlength{\fboxrule}{0.5pt}%
      \fbox{\includegraphics[width=0.7\textwidth]{figures/ambro-example.png}}
      }
      \caption{Our system \gui\ shows the right-branching tree, a corresponding map, suggestions, and a list of items that the user opted to add to the itinerary.\label{fig:ambro-example}}
\end{figure*}
\vspace{-0.2cm}

\paragraph{Natural Language Understanding} 
\label{section:nlu}

For \nlu, we applied the \emph{simple incremental update model} (\sium) \cite{Kennington2017} which can produce a distribution over possible slots in a semantic frame. This \nlu\ works incrementally: it updates the distribution over the slot values as it receives words from the \asr. The model is formalized below:

\vspace{-0.5cm}
{
\begin{center}
\begin{equation}
   P(I|U) = \frac{1}{P(U)} P(I)\sum_{r\in R} P(U|R=r)P(R=r|I) 
\label{eq:disc1}
\end{equation}
\end{center}
}

Where $P(I|U)$ is the probability of the intent $I$ (i.e., a semantic frame slot) of the utterance $U$. $R$ is a mediating variable of \emph{properties}, which maps between aspects of $U$ and $I$. For example, \texttt{italian} is an intent $I$, which has properties \texttt{pasta}, \texttt{mediterranean}, \texttt{vegetarian}, etc. For training, the system learns a mapping between words in $U$ and the properties in $R$. For application, for each word in $U$, a probability distribution is produced over $R$ which is summed over for each $I$. In our experiments, most properties are pre-defined (which is common), but sometimes properties need to be discovered, e.g., for street names which are unique to an area or city. Our system can discover properties and make use of them without prior training data using a Levenshtein distance calculation between the property and word strings (similar to \cite{kennington-schlangen2016}). As the system interacts with the user, it learns mappings between words and properties autonomously. 

%That is, following \cite{kennington-schlangen2016}, $P(I|U)$ is the probability of the intent $I$ (i.e., a slot of a semantic frame) behind the speaker's (ongoing) utterance $U$. This is recovered using the mediating variable $R$, a set of \emph{properties} which map between aspects of $U$ and aspects of $I$. We opt for abstract properties here (e.g., the frame for the \texttt{restaurant} domain might be filled by a certain type of cuisine intent such as \texttt{italian} which has properties like \texttt{pasta}, \texttt{mediterranean}, \texttt{vegetarian}, etc.). The properties can be seen as a kind of simple ontology. For $P(R|I)$, probability is distributed uniformly over all properties that a given intent is specified to have. The mapping between properties and aspects of $U$ in $P(U|R)$ can be learned from data. During application, $R$ is marginalised over, resulting in a distribution over possible intents.\footnote{In \cite{Kennington2013a} the authors apply Bayes' Rule to allow $P(U|R)$ to produce a distribution over properties, which we adopt here.} This occurs at each word increment, where the distribution from the previous increment is combined via $P(I)$, keeping track of the distribution over time. We realize the sub-model $P(U|R)$ as a maximum entropy classifier that maps words to properties (i.e., the properties are the class labels).

%Properties ($R$; i.e., the ontology) have often pre-defined by system designers. This is generally acceptable in many domains, however because of the nature of our \pa, some aspects of some domains are not known before hand. For example, if one wishes to find a bus local to where the user is, then the names of the bus stop locations must be discovered dynamically, and in the case of \sium, the intents and properties must be identified and linked to each other (in general the names are the intents and words that make up the names become properties; e.g., a bus stop named \emph{Front Street and Capitol} would have the name as the intent and the following properties \texttt{front}, \texttt{street}, \texttt{capitol}). This kind of discovery of intents and properties is a part of grounding (i.e., establishing mutual understanding of a particular domain) which extends beyond previous work.

%Because we cannot know beforehand the intents $I$ and properties $R$ in some domains (e.g., buses as explained above), we have no prior examples of how those intents will be referred to by users. To mitigate this, we follow \cite{kennington-schlangen2016} and apply a simple rule to add in a-priori knowledge: if some (potentially newly discovered) $r\in R$ and $w \in U$ are such that $r\doteq w$  (where $\doteq$ is string equality; e.g., an intent has the property of \texttt{pasta} and the word \emph{pasta} is uttered), then we set $C(U$=$w|R$=$r)$=$1$. To allow for possible \asr\ confusions, we also apply $C(U$=$w|R$=$r)$= $1 - ld(w,r) / max(len(w),len(r))$, where $ld$ is the \emph{Levenshtein distance}. For all other $w$, $C(w|r)$=$0$. This results in a distribution $C$, which we renormalize and blend with learned distribution to yield $P(U|R)$. This allows the system to function robustly despite a \emph{cold start}; i.e., without any prior user history or training data.

\paragraph{Grounded Conversation with an Informative GUI}
\label{sec:grounded_conversation}

%The system described and evaluated in \cite{kennington-schlangen2016} had a simple \gui\ which incrementally updated as a user spoke to the \pa. We improve upon their \gui\ by assuming that our \pa\ is to be used in a situation where the user wishes to locate something relative to her position; for example, a restaurant, museum, or some other point of interest. 

Our \gui\ has a map (using the Google Search and Maps APIs), a list of suggestions, and an itinerary created by the user derived from the suggestions. Figure~\ref{fig:ambro-example} portrays this: the top half of the \gui\ is a map annotated with the location of suggested items (in this example, restaurants). If a user selects any item in the \emph{Suggestions} list (e.g., by tapping or clicking on it), it is added to the \emph{Itinerary} list for later reference.  

\emph{Grounding through the GUI:}\ \ Figure~\ref{fig:ambro-example} shows the state of the \gui\ for an example utterance \emph{I'm hungry for some medium-priced Japanese food}. The \gui\ updated (i.e., by expanding branches showing the options and filling the branch with one of those options, as in \texttt{price:medium}), thereby signaling to the user continual understanding (i.e., a backchannel beyond just showing the \asr\ transcription). Nodes colored in red denote where the user should focus her attention. The system is able to signal a clarification request by displaying  \redcircle \texttt{\textbf{japanese?}} as a branch of \texttt{cuisine}. This informs the user not only that there was misunderstanding, but exactly \emph{what part of the utterance} was misunderstood (in this case, it technically wasn't misunderstanding; rather, the system verified the intent of the user). To continue, a simple \emph{yes} would fill \texttt{cuisine} with \texttt{japanese} thereby completing the expected information needed for that particular intent type (i.e., restaurant). At that point, the system is as informed as it will be so the user can select from the list of suggestions, ranked by relevance to the request utterance. In the event that a clarification request is answered with \emph{no} (or some other negative response), the question mark goes away and the node is filled again in blue; i.e., \bluecircle \texttt{\textbf{japanese}}. In addition to the tree, the map incrementally updates as the user's utterance unfolds by displaying a list of suggestions ranked by relevance; the location of those suggested items is further annotated in the map with a relevant icon. As the request unfolds, the number of points of interest shrinks, resulting in a zooming-in effect of the map. Taken together, these visual cues provide several signals of ongoing system understanding to the user. 

At any point the user can restart by saying a reserved keyword (e.g., \emph{reset}) and at any point the user can ``backtrack'' by saying \emph{no} which unfills each slot one by one. For example, in Figure~\ref{fig:ambro-example}, if the user had uttered \emph{I'm hungry for some medium-priced Mexican food} and the system filled \texttt{price:medium} and \texttt{cuisine:japanese}, the user could say \emph{no} which would result in an expanded \texttt{cuisine} slot. This allows users to repair potential misunderstandings locally before the system performs a (potentially wrong) task or produces an incorrect response. 

% \textbf{Interactive Learning}\ \ 

% By assuming that the \gui\ displays an acceptable signal of understanding to the user, we further leverage the \gui\ as a way for the user to convey an overt supervision signal into the system, thereby allowing the system to improve. Instead of asking the user for feedback via speech (e.g., \emph{Did I do what you wanted?}) which takes the user away from her task, the system temporarily displays a feedback request when the all of the slots in the tree are filled (e.g., below the tree) of \emph{Is that what you wanted?} with two buttons \emph{Yes} and \emph{No}. When the user selects \emph{Yes}, then the \gui\ informs \nlu\ by sending it the filled slot values and the utterance that led to the filled tree (if the user ignores the buttons, they fade away after 7 seconds). The \nlu\ then uses this information to improve its mapping between words in utterances (i.e., updating $P(U|R)$ as explained above) and resets the \gui\ tree. 

\emph{Autonomous Learning:}\ \ Our system further improves upon previous work by leveraging the \gui\ to learn as it interacts. We accomplish this by collecting the words of a completed utterance and corresponding filled slots then informing the \nlu\ that the utterance led to the filled slots--effectively providing an additional positive training example for the \nlu. The \nlu\ can then improve its probabilistic mapping between words and slot values;  i.e., through updating the sub-model $P(U|R)$ by retraining the classifier with the new information. This is a useful addition because the system designer could not possibly know beforehand all the possible utterances and corresponding intents for all users; this effectively allows the system to begin from scratch with little or no training data. It also allows the system to adapt (i.e., establish common ground) to user preferences as certain words denote certain items (e.g., \emph{noodles} could mean Japanese ramen for one user, or Italian pasta for another). Our system has provisions for providing autonomous learning by updating the \nlu\ using the filled slot values and the utterance when the user selects an item in the \emph{Suggested} list to add it to the \emph{Itinerary}. This allows the system to learn without interrupting the user's productivity with an explicit feedback request.

\section{Experiment}
\label{section:experiments}

This section explains a user evaluation performed on our \pa. Our \pa\ has provisions for finding information in the following domains: \emph{art galleries}, \emph{bars}, \emph{bus stations}, \emph{museums}, \emph{parking lots}, and \emph{restaurants}. These affordances are clearly visible to the users when they first see the \pa\ \gui.  Users interacted with one of two versions of our \pa: \emph{baseline} or \emph{autonomous learning}. Both versions were the same in that they discovered possible intents (in this experiment, only \texttt{bus station}), applied the same \gui\ (i.e., the annotated map and right-branching tree)  displayed selectable options which are added to an itinerary when selected. The \emph{autonomous learning} version improved as explained above. To allow for greater participation diversity, we made our system available through a web interface and posted the link on various social media sites.

%For each user, the system was reset to its original setting so each user who interacted with it was confronted with the same system. We did this to preserve the integrity of our experiment (i.e., by holding the starting settings constant) and to focus on grounding with individual users instead of general system improvement. We leave for future work a system that learns and improves from all users with which it interacts. 

\paragraph{Task \& Procedure} 

Participants were asked to use Chrome Browser on a non-mobile device (e.g., a laptop) with a working microphone. Participants took part in the study completely online by directing their browsers to an informed disclosure about the nature of the study, then instructions were given which are simplified as follows: you have been living in a city for a few months and a (fictitious) friend named Samantha wishes to visit you for a weekend. Use our \pa\ to plan out an itinerary for your friend's visit. We chose Boise, Idaho (U.S.A.) as the location for participants to explore (in future work, we will allow participants to set their language and location).

After reading and agreeing to the instructions, the participants were directed to our \pa\ system with which they interacted in their web browser via speech (we used Google \asr, which works incrementally). At any point, they could add candidate items suggested by our \pa\ into the \emph{Itinerary} list. This constituted phase one. After three minutes, a message popped up, showing their itinerary and a request that they re-create the same itinerary again for another friend.  The purpose of this is to see if the system had learned anything about their individual preferences or way of expressing their intent as they recreated their original itinerary. 

After they acknowledged the pop-up by clicking \emph{OK}, their itinerary was cleared and they were again able to interact with the system, thereby beginning phase two. They were given another three minutes to complete phase two, for a total of six minutes of interacting with our \pa. Afterwards, they were taken to a questionnaire about their experience with our \pa, followed by a form for them to enter for a gift card drawing, and finally a debriefing. This task favors a system that can suggest possibilities optimized for breadth; i.e., filling a diverse itinerary. Even though we wish to show how our system can ground autonomously, we opted for this task because it represents a realistic scenario beyond previous work. In total, 15 participants took place in our study and filled out the questionnaire, 8 for the baseline settings and 7 for the autonomous learning setting. 

%\cite{kennington-schlangen2016}, for example, evaluated a similar system with a more basic \gui, using very constrained and repetitive tasks in a lab setting. This, of course, makes direct observation of the participants impossible; we can only infer aspects of their interaction through the logs and questionnaires.

\paragraph{Metrics}

We report subjective and objective scores. We report objective measures for the following derived from system logs:
{\small
\begin{itemize}
 \item average length of utterances
 \item number of items added to the itinerary for the first phase
 \item fscore between itineraries in the two phases (where the itinerary from phase one is the expected itinerary for phase two)
 \item number of times the user had to \emph{reset} the \gui\ 
 \item number of times the user had to backtrack
 \item number of times the system applied improvements 
\end{itemize}
}

The subjective scores come from the questionnaires where participants responded using a 5-point Likert scale (the italicized portion is a shortened version of the question that we use in the results table below):
{\small
\begin{itemize}
 \item \emph{like the map} - I liked how the screen showed the map and the assistant at the same time.
 \item \emph{tree representation} - The assistant ``tree" could have been better represented in some other way.
 \item \emph{worked as expected} - I almost always got the results I wanted.
 \item \emph{intuitive} - the \pa\ was easy and intuitive to use. 
 \item \emph{noticed it improved} - I had the impression that it was improving. 
% \item \emph{knew what to do} - I always understood what was required for me to do.
 \item \emph{speak/pause} - I didn't know when to speak or pause.
 \item \emph{system predict better} - It could better predict what I wanted.
 \item \emph{appeared to understand} - It appeared understand what I said.
 \item \emph{natural interaction} - I felt that the interaction was more natural than the other personal assistants I have used.
 \item \emph{fix misunderstandings} - I liked that I could fix misunderstandings quickly.
\end{itemize}
}

We hypothesize that the \emph{autonomous} version of the system will result in better results than the baseline system which makes no attempt at learning or improvement. For subjective measures, we hypothesize that the overall experience of both versions will be positive (in fact, as a sanity check, for some questions and measures we expect the scores for both versions to be very similar), but overall the impression that the system improved should be higher for the autonomous learning settings. 

% \todo{we don't include the number of utterances because the ASR is returning incremental results}

\subsection{Results}

%In this section we report the results of our objective and subjective measures.

\paragraph{Objective} 

Table~\ref{tab:objscores} shows the objective results as averaged over all. The results show that, in general, the two systems produce similar results, as expected. The important difference is in the fscore, which shows how well the itineraries of the two phases match: the itinerary fscore between the first and second phases for the autonomous system is much higher than it is for the baseline system. We conjecture that this is due to the system learning during the first phase what kinds of items the user added to the itinerary (as illustrated by the average number of improvements done by the autonomous version). During phase two when the users were required to make the same itinerary, the autonomous system had a stronger mapping of utterances and previously selected items, thereby predicting to a small degree what their preferences were (which, as explained above, is a form of grounding).

\begin{table}
\centering
{\small
 \begin{tabular}{|r|c|c|}
\hline
  \textbf{item}         & \textbf{baseline}  & \textbf{autonomous} \\
\hline
\textbf{avg utt len}*     & 1.63 (0.69) &  2.86 (1.51)\\  
\textbf{avg \# itinerary items}  & 2.5 (3.6) &  1.75 (1.78)\\
\textbf{avg itinerary fscore}& 0.04 (0.07) &  0.5 (0.5)\\
\textbf{avg \# reset}*    & 11.6 (26.2) & 6.12 (10.2)\\
\textbf{avg \# no}*       & 2.6 (6.9)  & 2.18 (3.23)\\
\textbf{avg \# improvements}    &  0 (0)   & 6.25 (3.9)\\

 \hline
\end{tabular}
}
\caption{Objective results: avg. (std). Asterisks denote items where lower scores are better.}
\label{tab:objscores}
\end{table}


\paragraph{Subjective} 

Table~\ref{tab:subjscores} shows the subjective scores for the questionnaire averaged over all participants with standard deviation in parentheses (questions with an asterisk denote questions where lower scores are better). Overall, the subjective scores do not show a strong preference for either system (a t-test revealed no statistical significance using a Bonrefoni correction of 12); though both systems are rated positively. The users did like that the map directly showed points of interest and they liked the ability to reset at anytime. Though they did not have the impression that the autonomous version was improving while they interacted, they did notice that the autonomous version predicted what they wanted more than the baseline system. 
\begin{table}[h]
\centering
 {\small
 \begin{tabular}{|r|c|c|}

\hline
\textbf{question}  & \textbf{baseline}  & \textbf{autonomous} \\
\hline
\textbf{I like the map}             & 4.5  (0.7) &  4.3 (0.9) \\  
\textbf{tree representation}*            & 3.1  (1.3) &  3.0 (1.6) \\
% \textbf{obvious}                  & 3.0  (1.2) &  3.6 (1.14) \\
\textbf{worked as expected}       & 3.0  (1.3) &  3.0 (1.3) \\
\textbf{intuitive}                & 3.1  (0.9) &  3.3 (1.2) \\
% \textbf{liked if it und.}         & 3.0  (1.2) &  2.7 (1.7)\\
\textbf{I noticed it improved}       & 3.0  (1.0) &  2.9 (1.2) \\
% \textbf{did right thing after CR} & 2.8  (1.5) &  2.4 (1.14)\\
%\textbf{I knew what to do}        & 3.4  (1.3) &  2.4 (1.1) \\
\textbf{speak/pause}*              & 3.4  (1.4) &  3.3 (1.6)\\
\textbf{predict better}*     & 3.1  (1.2) &  2.5 (1.0)\\
\textbf{appeared to understand}       & 3.0  (1.2) &  3.3 (1.4)\\
\textbf{natural interaction}      & 2.5  (1.2) &  2.9 (1.0) \\
\textbf{fix misunderstandings}              & 3.4  (1.0) &  3.0 (1.4) \\

\hline

\end{tabular}
}
\caption{Subjective results from questionnaires: avg. (std). Asterisks denote questions where lower scores are better.}
\label{tab:subjscores}
\end{table}


\section{Conclusions \& Future Work}

The results are positive overall: the system is useful and allows users to fill an itinerary using speech. Users were able to recreate their itineraries with the autonomous system much more accurately than with the baseline system Minimal grounding indeed took place through the \gui\ by the tree and map, both of which updated incrementally as the users' utterances unfolded, by properties (i.e., ontology) discovery by the system, and by improving the mapping between utterances and properties. For future work, we will leverage our system to autonomously improve the dialogue manager.

%The overall positive results show that the system is robust despite the lack of training data. 

%Beyond the above questions, users specified that they would use this as an application on a mobile device (avg 3.3, std 1.5). It is further worth noting that  we asked participants what frustrates them most about current personal assistants (we provided four possible responses; participants could select at least two). The most common responses were that systems (1) do not understand what the users want and (2) that users are unable to correct problems that arise mid-sentence. Our system helps (1) by having provisions for (2) for both system versions that we evaluated. 

%For future work, we will add speech as a modality for the system to interact with the user at certain times. We will also dig deeper into the dialogue manager so it too improves interactively using reinforcement learning and using deep learning. 

%\paragraph{Acknowledgements} 

%\section*{Appendix}
%\label{section:appendix}


\bibliographystyle{SICHI-Reference-Format}
\balance
\bibliography{refs}

\end{document}
