%%% -*-BibTeX-*-
%%% Do NOT edit. File created by BibTeX with style
%%% ACM-Reference-Format-Journals [18-Jan-2012].

\begin{thebibliography}{00}

%%% ====================================================================
%%% NOTE TO THE USER: you can override these defaults by providing
%%% customized versions of any of these macros before the \bibliography
%%% command.  Each of them MUST provide its own final punctuation,
%%% except for \shownote{}, \showDOI{}, and \showURL{}.  The latter two
%%% do not use final punctuation, in order to avoid confusing it with
%%% the Web address.
%%%
%%% To suppress output of a particular field, define its macro to expand
%%% to an empty string, or better, \unskip, like this:
%%%
%%% \newcommand{\showDOI}[1]{\unskip}   % LaTeX syntax
%%%
%%% \def \showDOI #1{\unskip}           % plain TeX syntax
%%%
%%% ====================================================================

\ifx \showCODEN    \undefined \def \showCODEN     #1{\unskip}     \fi
\ifx \showDOI      \undefined \def \showDOI       #1{{\tt DOI:}\penalty0{#1}\ }
  \fi
\ifx \showISBNx    \undefined \def \showISBNx     #1{\unskip}     \fi
\ifx \showISBNxiii \undefined \def \showISBNxiii  #1{\unskip}     \fi
\ifx \showISSN     \undefined \def \showISSN      #1{\unskip}     \fi
\ifx \showLCCN     \undefined \def \showLCCN      #1{\unskip}     \fi
\ifx \shownote     \undefined \def \shownote      #1{#1}          \fi
\ifx \showarticletitle \undefined \def \showarticletitle #1{#1}   \fi
\ifx \showURL      \undefined \def \showURL       #1{#1}          \fi

\bibitem{Aist2006}
{Gregory Aist}, {James Allen}, {Ellen Campana}, {Lucian Galescu}, {Carlos
  Gallo}, {Scott Stoness}, {Mary Swift}, {and} {Michael Tanenhaus}. 2006.
\newblock \showarticletitle{{Software architectures for incremental
  understanding of human speech}}. In {\em Proceedings of CSLP}. 1922----1925.
\newblock
\showISBNx{9781604234497}


\bibitem{Aistetal:incrunder-short}
{Gregory Aist}, {James Allen}, {Ellen Campana}, {Carlos~Gomez Gallo}, {Scott
  Stoness}, {and} {Mary Swift}. 2007.
\newblock \showarticletitle{{Incremental understanding in human-computer
  dialogue and experimental evidence for advantages over nonincremental
  methods}}. In {\em Pragmatics}, Vol.~1. Trento, Italy, 149--154.
\newblock


\bibitem{Asri2014}
{Layla~El Asri}, {Romain Laroche}, {Olivier Pietquin}, {and} {Hatim Khouzaimi}.
  2014.
\newblock \showarticletitle{{NASTIA: Negotiating Appointment Setting
  Interface}}. In {\em Proceedings of LREC}. 266--271.
\newblock


\bibitem{Chai2014}
{Joyce~Y Chai}, {Lanbo She}, {Rui Fang}, {Spencer Ottarson}, {Cody Littley},
  {Changsong Liu}, {and} {Kenneth Hanson}. 2014.
\newblock \showarticletitle{{Collaborative effort towards common ground in
  situated human-robot dialogue}}. In {\em Proceedings of the 2014 ACM/IEEE
  international conference on Human-robot interaction}. Bielefeld, Germany,
  33--40.
\newblock
\showISBNx{9781450326582}
\showISSN{21672148}
\showDOI{%
\url{http://dx.doi.org/10.1145/2559636.2559677}}


\bibitem{clarkschaefer:contrdis}
{Herbert~H. Clark} {and} {Edward~F. Schaefer}. 1989.
\newblock \showarticletitle{{Contributing to discourse}}.
\newblock {\em Cognitive Science\/} {13}, 2 (1989), 259--294.
\newblock
\showISBNx{0364-0213; 1551-6709}
\showISSN{03640213}
\showDOI{%
\url{http://dx.doi.org/10.1016/0364-0213(89)90008-6}}


\bibitem{Dethlefs2015}
{Nina Dethlefs}, {Helen Hastie}, {Heriberto Cuay{\'{a}}huitl}, {Yanchao Yu},
  {Verena Rieser}, {and} {Oliver Lemon}. 2016.
\newblock \showarticletitle{{Information density and overlap in spoken
  dialogue}}.
\newblock {\em Computer Speech and Language\/}  {37} (2016), 82--97.
\newblock
\showISSN{10958363}
\showDOI{%
\url{http://dx.doi.org/10.1016/j.csl.2015.11.001}}


\bibitem{Edlund2008b}
{Jens Edlund}, {Joakim Gustafson}, {Mattias Heldner}, {and} {Anna Hjalmarsson}.
  2008.
\newblock \showarticletitle{{Towards human-like spoken dialogue systems}}.
\newblock {\em Speech Communication\/} {50}, 8-9 (2008), 630--645.
\newblock
\showISSN{01676393}
\showDOI{%
\url{http://dx.doi.org/10.1016/j.specom.2008.04.002}}


\bibitem{Hough2017a}
{Julian Hough} {and} {David Schlangen}. 2017.
\newblock \showarticletitle{{A Model of Continuous Intention Grounding for
  HRI}}. In {\em Proceedings of The Role of Intentions in Human-Robot
  Interaction Workshop}.
\newblock


\bibitem{kennington-kousidis-schlangen:2014:W14-43}
{Casey Kennington}, {Spyros Kousidis}, {and} {David Schlangen}. 2014.
\newblock \showarticletitle{{InproTKs: A Toolkit for Incremental Situated
  Processing}}. In {\em Proceedings of the 15th Annual Meeting of the Special
  Interest Group on Discourse and Dialogue (SIGDIAL)}. Association for
  Computational Linguistics, Philadelphia, PA, U.S.A., 84--88.
\newblock
\showURL{%
\url{http://www.aclweb.org/anthology/W14-4312}}


\bibitem{kennington-schlangen2016}
{Casey Kennington} {and} {David Schlangen}. 2016.
\newblock \showarticletitle{{Supporting Spoken Assistant Systems with a
  Graphical User Interface that Signals Incremental Understanding and
  Prediction State}}. In {\em Proceedings of the 17th Annual Meeting of the
  Special Interest Group on Discourse and Dialogue}. Association for
  Computational Linguistics, Los Angeles, 242--251.
\newblock
\showURL{%
\url{http://www.aclweb.org/anthology/W16-3631}}


\bibitem{Kruijff2012}
{Geert-Jan~M Kruijff}. 2012.
\newblock \showarticletitle{{There is no common ground in human-robot
  interaction}}. In {\em Proceedings of SemDial}.
\newblock


\bibitem{Schlangen2011}
{David Schlangen} {and} {Gabriel Skantze}. 2011.
\newblock \showarticletitle{{A General, Abstract Model of Incremental Dialogue
  Processing}}. In {\em Dialogue {\&} Discourse}, Vol.~2. 83--111.
\newblock
\showISBNx{9781932432169}
\showISSN{2152-9620}
\showDOI{%
\url{http://dx.doi.org/10.5087/dad.2011.105}}


\bibitem{skantze2010sigdial}
{Gabriel Skantze} {and} {Anna Hjalmarsson}. 1991.
\newblock \showarticletitle{{Towards Incremental Speech Production in Dialogue
  Systems}}. In {\em Word Journal Of The International Linguistic Association}.
  Tokyo, Japan, 1--8.
\newblock


\bibitem{Skantze2009}
{Gabriel Skantze} {and} {David Schlangen}. 2009.
\newblock \showarticletitle{{Incremental dialogue processing in a
  micro-domain}}.
\newblock {\em Proceedings of the 12th Conference of the European Chapter of
  the Association for Computational Linguistics on EACL 09\/} April (2009),
  745--753.
\newblock
\showISBNx{9781932432169}
\showDOI{%
\url{http://dx.doi.org/10.3115/1609067.1609150}}


\bibitem{Spivey_2002tw}
{Michael~J. Spivey}, {Michael~K. Tanenhaus}, {Kathleen~M. Eberhard}, {and}
  {Julie~C. Sedivy}. 2002.
\newblock \showarticletitle{{Eye movements and spoken language comprehension:
  Effects of visual context on syntactic ambiguity resolution}}.
\newblock {\em Cognitive Psychology\/} {45}, 4 (2002), 447--481.
\newblock
\showISBNx{0010-0285}
\showISSN{00100285}
\showDOI{%
\url{http://dx.doi.org/10.1016/S0010-0285(02)00503-0}}


\bibitem{Tanenhaus1995}
{Michael Tanenhaus}, {Michael Spivey-Knowlton}, {Kathleen Eberhard}, {and}
  {Julie Sedivy}. 1995.
\newblock \showarticletitle{{Integration of visual and linguistic information
  in spoken language comprehension.}}
\newblock {\em Science (New York, N.Y.)\/} {268}, 5217 (1995), 1632--1634.
\newblock
\showISBNx{0036-8075}
\showISSN{0036-8075}
\showDOI{%
\url{http://dx.doi.org/10.1126/science.7777863}}


\end{thebibliography}
