\documentclass{sigchi}
% \usepackage{acl2016}

% \CopyrightYear{2016}
%\setcopyright{acmcopyright}
% \setcopyright{acmlicensed}


\usepackage{times}
% \usepackage[round]{natbib}
\usepackage{latexsym}
\usepackage[utf8]{inputenc}
% \usepackage[font=small,labelfont=bf]{caption}
\usepackage{amsmath}
\usepackage{multirow}
\usepackage{appendix}
\usepackage{url}
\usepackage{tikz}
% \usepackage{avm}
% \avmfont{\sc}
% \avmoptions{sorted}
% \avmvalfont{\rm}
% \avmsortfont{\scriptsize\it}
\usepackage{remreset}
\usepackage{pifont}
% \newcommand{\cmark}{\ding{53}}%
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{verbatim} 
\usepackage{linguex}
\usepackage{qtree}

%\usepackage{algorithm}% http://ctan.org/pkg/algorithms
%\usepackage{algpseudocode}% http://ctan.org/pkg/algorithmicx
\usepackage{amsmath,amsfonts,amssymb}

\newcommand{\argmax}{\operatornamewithlimits{argmax}}

\usepackage{color}
\newcommand{\das}[1]{{\color{red}\emph{//das: #1//}}}
\newcommand{\crk}[1]{\emph{//crk: #1//}}
\newcommand{\todo}[1]{\textcolor{green}{\emph{//todo: #1//}}}


\newcommand{\sds}[0]{\textsc{sds}}
\newcommand{\nlu}[0]{\textsc{nlu}}
\newcommand{\rmrs}[0]{\textsc{rmrs}}
\newcommand{\ep}[0]{\texttt{EP}}
\newcommand{\inprotk}[0]{\textsc{InproTK}}
\newcommand{\sium}[0]{\textsc{sium}}
\newcommand{\asr}[0]{\textsc{asr}}
\newcommand{\dm}[0]{\textsc{dm}}
\newcommand{\ui}[0]{\textsc{gui}}
\newcommand{\iu}[0]{\textsc{iu}}
\newcommand{\rr}[0]{\textsc{rr}}
\newcommand{\pa}[0]{\textsc{pa}}
\newcommand{\gui}[0]{\textsc{gui}}
\newcommand{\rs}[0]{\textsc{rs}}


%Document Head
% \dochead{CLV2 Class File Manual}

%\title{An Adaptive, Incremental Personal Assistant that Graphically Signals Speech Understanding}
\title{amBrOISEa: a Cold-start, Speech-driven,\\ Point-of-Interest Recommender}

\author{Casey Kennington \and Aprajita Shukla \\ \\ Boise State Unviversity \\ firstnamelastname@boisestate.edu \and Maria Soledad Pera  }
% \affil{Boise State University\\firstnamelastname@boisestate.eduy}


\begin{document}
% \affil{Publishing / SPi}

\maketitle

% abstract should be 150-250 words
\begin{abstract}
We describe a demo for a speech-driven, point-of-interest recommender system called amBrOISEa which functions despite cold start scenarios where there might not be a user profile. AmBrOISEa has provisions for improvement as it interacts with users.
\end{abstract}

\section{Introduction}
\label{section:intro}

One challenge of \emph{recommender systems} (\rs) is so-called \emph{cold start}; i.e., the \rs\ has no prior history to predict the preferences of the a user. \cite{Levi2012} addressed cold start by grouping known users and fitting the new user into one of those groups. \cite{Zhang2014} used contextual features beyond ratings to improve recommendations made to new users. Though these systems show promise, the cold start problem is far from solved. 

Cold start is also an issue in \emph{spoken dialogue systems} (\sds) research when, for example, a personal assistant (e.g., Siri or Alexa) must perform useful actions (e.g., estimate commute time or set a timer) using speech as the only interface, yet individuals have sometimes unusual or nuanced ways of expressing themselves. Though some work in \sds\ research has attempted to bootstrap systems with little or no training data (i.e., cold start) \cite{kennington-schlangen2016}, they are generally unable to improve their language understanding as they interact with individual users. 

In this demo, we offer a solution to the cold start problem by treating recommendation problem as a \emph{reference resolution} task, which is common in \sds\ research. We assert that this approach is appropriate because users express their intent via spoken utterances, which are ongoing descriptions of what they are looking for (though certainly they are often unsure) with the goal of resolving their spoken utterances to a single, most appropriate entity (or, in our case, a ranked list). We explain amBrOISEa in the following section. 

\section{System Description}
\label{section:related_work}

\begin{figure} %[ht]
  \centering
      \includegraphics[width=0.3\textwidth]{figures/sig16-overview.pdf}	
      \caption{System overview: \asr\ which transcribes speech into words, \nlu\ which produces a semantic representation (realized as a set of semantic slots), \dm\ which makes high-level decisions, and a \gui.\label{fig:sig16}}
\end{figure}

AmBrOISEa builds directly off of the \sds\ described in \cite{kennington-schlangen2016} which was composed of four main components: automatic speech recognition (\asr) which transcribes spoken utterances into text strings, natural language understanding (\nlu; explained below), a dialogue manager (\dm) using OpenDial \cite{Lison2015a} which determined when amBrOISEa should \texttt{select}, \texttt{wait} for more information, \texttt{request} confirmation, or \texttt{confirm} a confirmation request, and the final component was a \gui\ in the form of a right-branching tree which conveyed ambrosia's state of understanding to the user. Figure~\ref{fig:sig16} conceptually shows these components and how the information flows between them. As amBrOISEa attempts to address the cold start problem by leveraging the \gui\ to improve the \nlu, we briefly explain both.

\begin{figure*}[ht]
  \centering
  {
    \setlength{\fboxsep}{0pt}%
\setlength{\fboxrule}{0.5pt}%
      \fbox{\includegraphics[width=0.6\textwidth]{figures/ambro-example.png}}
      }
      \caption{The \gui\ shows the right-branching tree, a corresponding map, recommendations, and a list of items that the user opted to add to an itinerary.\label{fig:ambro-example}}
\end{figure*}


\textbf{Natural Language Understanding} \ \ For \nlu, we applied the \emph{simple incremental update model} (\sium) \cite{Kennington2017} which can produce a distribution over possible slots in a semantic frame, formalized below:

\vspace{-0.25cm}
{\small
\begin{center}
\begin{equation}
   P(I|U) = \frac{1}{P(U)} P(I)\sum_{r\in R} P(U|R=r)P(R=r|I) 
\label{eq:disc1}
\end{equation}
\end{center}
}

That is, $P(I|U)$ is the probability of recovering the intent $I$ (i.e., the unknown intended entity of the user) given the utterance $U$. The sub-model $P(R|I)$ links those entities with properties ($R$; e.g., in the \texttt{restaurant} domain, the intent $I$ \texttt{italian} has properties like \texttt{pasta}, \texttt{mediterranean}, \texttt{vegetarian}, etc.). This can be seen as a simple ontology that a system designer can determine. The other sub-model $P(U|R)$ maps those properties to aspects of the utterance (i.e., the words). The system can function on a cold start by allowing $P(R|U)$ to produce a probability of 1 when an $r \in R$ and $w \in U$ match (e.g., $w=$ \emph{pasta} matches the property $r=$ \texttt{pasta}) which works for most users. However, as noted above, some users express themselves differently (e.g., \emph{noodles} could mean Italian \emph{pasta} or Japanese \emph{ramen}). As a new user interacts with amBrOISEa the $P(R|U)$ mapping can improve by providing examples of how a new user expresses herself through an utterance $U$ and the intended action \emph{as the amBrOISEa interacts} with that user. Determining when to inform amBrOISEa that a new user's utterance should be used to improve amBrOISEa is determined by how the user interacts with the \gui, which we will now explain. 

\textbf{Graphical User Interface} \ \ The \gui\ assumes that our system will be used in a situation where the user wishes to locate a point of interest. We augment the \gui\ by adding a map (using the Google Search and Maps APIs), a list of recommendations, and a list of recommendations that the user indicated they wanted saved for later reference (i.e., a kind of itinerary). Figure~\ref{fig:ambro-example} portrays this: the top half of the \gui\ is a map annotated with the location of suggested items. If a user selects any item in the \emph{Suggestions} list (e.g., by clicking on it), it is added to the \emph{Itinerary} list for later reference. 

Figure~\ref{fig:ambro-example} shows the state of the \gui\ for an example utterance \emph{I'm hungry for some medium-priced Japanese food}. The \gui\ updated (i.e., by expanding branches showing the options and filling the branch with one of those options, as in \texttt{price:medium}), thereby signaling to the user continual language understanding. The system is able to signal a clarification request by displaying \texttt{japanese?} as a branch of \texttt{cuisine}. Nodes colored in red denote where the user should focus her attention. To continue, a simple \emph{yes} would fill \texttt{cuisine} with \texttt{japanese} thereby completing the expected information needed for that particular intent type (i.e., restaurant). At any point, the user can select from the list of recommendations. In the event that a clarification request is answered with \emph{no} (or some other negative response), the question mark goes away and the node is filled again in blue. In addition to the tree, the map updates as the user's utterance unfolds by displaying a list of recommendations ranked by relevance; the location of those suggested items is further annotated in the map with a relevant icon. Taken together, these visual cues provide several signals of ongoing system understanding to the user. At any point the user can restart by saying a reserved keyword (e.g., \emph{reset}) and at any point the user can ``backtrack'' by saying \emph{no} which unfills each slot one by one. This allows users to repair potential misunderstandings locally before amBrOISEa performs a (potentially wrong) task or produces an incorrect response. 

Learning from cold start is accomplished by collecting the words of a completed utterance and corresponding filled slots then informing the \nlu\ that the utterance led to the filled slots--effectively providing an additional positive training example for the \nlu. The \nlu\ can then improve its probabilistic mapping between words and properties;  i.e., by updating the sub-model $P(U|R)$ (e.g., if someone says \emph{cheap} instead of \emph{low priced}). This is a useful addition because the system designer could not possibly know beforehand all the possible utterances and corresponding intents for all users, effectively allowing amBrOISEa to begin from scratch with little or no training data. It also allows amBrOISEa to adapt to user preferences as certain words denote certain items (e.g., \emph{noodles} could mean Japanese ramen for one user, or Italian pasta for another). Our system has provisions for providing autonomous learning by sending the filled slot values and the utterance (which may not be complete) when the user selects a suggested item to add it to the \emph{Itinerary} list and resets the \gui\ tree. This allows amBrOISEa to learn without interrupting the user's productivity with an explicit feedback request.


\section{Applicability}

We intend to use amBrOISEa as a \rs\ for travelers who visit a new city to help them construct a list of possible points of interest. It is designed to work with a minimal or non-existent a user profile, yet still be initially useful to any user. Prolonged use would result in better understanding and recommendations. A demo of amBrOISEa is available online at \url{http://ambroisea1.herokuapp.com}.
.

{\small

\bibliographystyle{SIGCHI-Reference-Format}
\bibliography{refs}
}
\end{document}

