var margin = {top: 20, right: 200, bottom: 20, left: 120},
	    width = 960;
	    height = 300;
	
var duration = 350;

var tree = d3.layout.tree()
    .size([height, width]);

var diagonal = d3.svg.diagonal()
    .projection(function(d) { return [d.y, d.x]; }); 

var svg = d3.select("#gui").append("svg")
    .attr("width", width + margin.right + margin.left)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


function updateTree(source) {
      	  // Compute the new tree layout.
      	  var nodes = tree.nodes(root).reverse(),
      	  links = tree.links(nodes);

      	  // Normalize for fixed-depth.
      	  nodes.forEach(function(d) { d.y = d.depth * 200; });

      	  // Update the nodes…
      	  var node = svg.selectAll("g.node")
      	      .data(nodes, function(d) { return d.name; });

      	  // Enter any new nodes at the parent's previous position.
      	  var nodeEnter = node.enter().append("g")
      	      .attr("class", "node")
      	      .attr("transform", function(d) {
          	  if (d.parent) {
          	  	return "translate(" + d.parent.y + "," + d.parent.x + ")"; 
          	  }
          	  else {
          		return "translate(" + source.y0 + "," + source.x0 + ")"; 
              }
      	      });

      	  nodeEnter.append("circle")
               .attr("r", 4.5)
               .style("stroke", function(d) { return d.type; })
   			   .style("fill", function(d) { return d.level; }); 	

      	  nodeEnter.append("text")
      	  .style("fill-opacity", 1)
      	      .attr("x", function(d) { return d.children || d._children ? -4 : 4; })
      	      .attr("dy", ".35em")
      	      .style("font-size","18px")
      	      .style("text-anchor", function(d) { return d.children ? "end" : "start"; })
			  .each(function (d) {
          	    var arr = d.name.split("_");
          	    var nname = "";
          	    for (i = 0; i < arr.length; i++) {
          	    	nname += arr[i] + " ";
          	    }
          	    d3.select(this).append("tspan")
		          .text(nname)
		          //.attr("dy", i ? "1.2em" : 0)
		          .attr("x",  function(d) { return d.children || d._children ? -4 : 4; })
		          .attr("class", "tspan" + i);
          	  });  

      	  // Transition nodes to their new position.
      	  var nodeUpdate = node.transition()
      	      .duration(duration)
      	      .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });
		      	  	      

      	  nodeUpdate.select("circle")
      	      .attr("r", 3)
	          .style("stroke", function(d) { return d.type; })
	  		  .style("fill", function(d) { return d.level; }); 

      	  
      	  nodeUpdate.select("text")
      	    .style("font-size","18px")
      	  	.style("text-anchor", function(d) { return d.children ? "end" : "start"; })
		    .style("fill-opacity", 1)      

      	  // Transition exiting nodes to the parent's new position.
      	  var nodeExit = node.exit().transition()
//       	      .duration(duration)
      	      .remove();
  	      
      	    // Update the links…
			 var link = svg.selectAll("path.link")
			      .data(links, function(d) { return d.target.name; });

			  // Enter any new links at the parent's previous position.
			  link.enter().insert("path", "g")
			      .attr("class", "link")
			      .attr("d", function(d) {
			        var o = {x: d.source.x, y: d.source.y};
			        return diagonal({source: o, target: o});
			      });
			
			  // Transition links to their new position.
			  link.transition()
			      .duration(duration)
			      .attr("d", diagonal);
			
			  // Transition exiting nodes to the parent's new position.
			  link.exit().transition()
			      .duration(duration)
			      .attr("d", function(d) {
			        var o = {x: d.source.x, y: d.source.y};
			        return diagonal({source: o, target: o});
			      })
			      .remove();
		      
      	  // Stash the old positions for transition.
      	  nodes.forEach(function(d) {
      	    d.x0 = d.x;
      	    d.y0 = d.y;
      	  });

        }