package app;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.sound.sampled.UnsupportedAudioFileException;


import edu.cmu.sphinx.util.props.ConfigurationManager;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Component;
import edu.cmu.sphinx.util.props.S4ComponentList;
import inpro.apps.SimpleReco;
import inpro.apps.SimpleText;
import inpro.apps.util.RecoCommandLineParser;
import inpro.incremental.PushBuffer;
import inpro.incremental.processor.TextBasedFloorTracker;
import inpro.incremental.source.GoogleASR;
import inpro.incremental.source.IUDocument;
import inpro.incremental.source.SphinxASR;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.WordIU;
import jetty.AdvancedDiaTreeCreator;
import jetty.DiaTreeSocket;
import jetty.JettyServer;
import log.InteractionLogger;
import model.Constants;
import model.CustomFunctionRegistry;
import module.INLUModule;
import util.ClientUtils;

public class Main {
	
//	@S4Component(type = SphinxASR.class)
//	public final static String PROP_CURRENT_HYPOTHESIS = "currentASRHypothesis";
	
	
	private void run(String apiKey) throws InterruptedException, PropertyException, IOException, UnsupportedAudioFileException {

		InteractionLogger.initConstants(false);

//		hypListeners = ps.getComponentList(PROP_HYP_CHANGE_LISTENERS, PushBuffer.class);
		
		AdvancedDiaTreeCreator creator = new AdvancedDiaTreeCreator(new DiaTreeSocket());
		JettyServer jetty = new JettyServer(creator);
		
//		for Sphinx ASR
//		SphinxASR webSpeech = (SphinxASR) cm.lookup(PROP_CURRENT_HYPOTHESIS);
//		RecoCommandLineParser rclp = new RecoCommandLineParser(new String[] {"-M"});
		
//		TextBasedFloorTracker textBasedFloorTracker = (TextBasedFloorTracker) cm.lookup(PROP_FLOOR_MANAGER);
//		final List<PushBuffer> hypListeners = ps.getComponentList(PROP_HYP_CHANGE_LISTENERS, PushBuffer.class);
//		IUDocument iuDocument = new IUDocument();
//		iuDocument.setListeners(hypListeners);
//		SimpleText.createAndShowGUI(hypListeners, textBasedFloorTracker);
		
//		for Google ASR
		
//		webSpeech = (GoogleASR) cm.lookup("googleASR");
//		RecoCommandLineParser rclp = new RecoCommandLineParser(new String[] {"-A", "-G", apiKey, "-Lp", "/home/ckennington/Desktop/out"});
//		RecoCommandLineParser rclp = new RecoCommandLineParser(new String[] {"-A" ,"-Lp", "/home/ckennington/Desktop/out"});
//		RecoCommandLineParser rclp = new RecoCommandLineParser(new String[] {"-G", apiKey, "-F", "file:///home/ckennington/git/gruber/Code/diatree/temp.wav", "-Lp", "/home/ckennington/Desktop/out"});
//		RecoCommandLineParser rclp = new RecoCommandLineParser(new String[] {"-G", apiKey, "-M", "-Lp", "/home/ckennington/Desktop/out"});
//		startGoogleASR(cm, rclp);
		
		
//		ClientUtils.openNewClient();
	}
	
	
//	private void startGoogleASR(ConfigurationManager cm, RecoCommandLineParser rclp) {
//		new Thread() {
//		public void run() {
//			
//			try {
//				SimpleReco simpleReco = new SimpleReco(cm, rclp);
//			while (true) {
//				try {
//					
//
//					new Thread(){ 
//						public void run() {
//							try {
//								System.out.println("RECOGNIZING ONCE");
//								simpleReco.recognizeOnce();
//							} 
//							catch (PropertyException e) {
//								e.printStackTrace();
//							} 
//							
//						}
//					}.start();
//					
//					Thread.sleep(20000);
//					webSpeech.shutdown();
////					simpleReco.shutdownMic();
//				}
//					
//				catch (InterruptedException e) {
//					e.printStackTrace();
//				}
//			}
//			
//			
//		} catch (PropertyException e1) {
//			e1.printStackTrace();
//		} catch (IOException e1) {
//			e1.printStackTrace();
//		} catch (UnsupportedAudioFileException e1) {
//			e1.printStackTrace();
//		}
//	}
//	}.start();
//	
//	}

	public static void main (String[] args) {
		try {
//			System.err.println("Using API KEY: " + args[0]);
			new Main().run("AIzaSyDXOjOCiM7v0mznDF1AWXXoR1ehqLeIB18");
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		} 
		catch (PropertyException e) {
			e.printStackTrace();
		} 
		catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} 
	}
	
//	public void notifyListeners(List<PushBuffer> listeners) {
//		if (edits != null && !edits.isEmpty()) {
//			//logger.debug("notifying about" + edits);
//			for (PushBuffer listener : listeners) {
//				listener.hypChange(null, edits);
//			}
//			edits = new ArrayList<EditMessage<IU>>();
//		}
//	}

}
