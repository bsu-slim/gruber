package model;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.eclipse.jetty.websocket.api.Session;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.cmu.sphinx.util.props.ConfigurationManager;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Component;
import edu.cmu.sphinx.util.props.S4ComponentList;
import inpro.apps.SimpleText;
import inpro.incremental.FrameAware;
import inpro.incremental.IUModule;
import inpro.incremental.PushBuffer;
import inpro.incremental.processor.TextBasedFloorTracker;
import inpro.incremental.source.GoogleASR;
import inpro.incremental.source.IUDocument;
import inpro.incremental.source.SphinxASR;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.IUList;
import inpro.incremental.unit.WordIU;
import inpro.util.TimeUtil;
import log.InteractionLogger;
import module.INLUModule;
import module.TreeModule;
import util.ClientUtils;

public class GruberInstance extends IUModule {

	@S4ComponentList(type = PushBuffer.class)
	public final static String PROP_HYP_CHANGE_LISTENERS = SphinxASR.PROP_HYP_CHANGE_LISTENERS;
	
	@S4Component(type = TextBasedFloorTracker.class)
	public final static String PROP_FLOOR_MANAGER = "textBasedFloorTracker";	
	
	@S4Component(type = SphinxASR.class)
	public final static String PROP_CURRENT_HYPOTHESIS = "currentASRHypothesis";
	
	@S4Component(type = INLUModule.class)
	public final static String INLU_MODULE = "inlu";
	protected INLUModule inlu;
	
	@S4Component(type = TreeModule.class)
	public final static String TREE_MODULE = "module";
	private TreeModule tree;
	
	private IUList<WordIU> chunkHyps;
	private long initialTime; // in milliseconds
	
	GoogleASR webSpeech;
	private PropertySheet ps;
//	private List<PushBuffer> hypListeners;
	List<EditMessage<IU>> edits = new ArrayList<EditMessage<IU>>();
	private Session session;
	private int ID;
	private int VID;
	private int trainCount;
	
	public GruberInstance(Session s, int id, int vid) throws PropertyException, MalformedURLException {
		setSession(s);
		setID(id);
		setVID(vid);
		trainCount = 0;
		InteractionLogger.logger.info(getID() + " NEW CONNECTION");
		InteractionLogger.logger.info(getID() + " vid=" + getVID());
		
//		start a new InproTK instance (which will also make a new instance of OpenDial for the DM, and SIUM for NLU)
		ConfigurationManager cm = new ConfigurationManager(new File("src/config/config.xml").toURI().toURL());
		cm.setGlobalProperty("sessionID", getID()+"");
		try {
			prepareFiles();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		ps = cm.getPropertySheet(PROP_CURRENT_HYPOTHESIS);
		
		initialTime = getTimestamp();
		
//		queue = (AudioStreamQueue) ps.getComponent(AUDIO_STREAM_QUEUE);
		chunkHyps = new IUList<WordIU>();
		
		//this block of code is for the text input gui
//		TextBasedFloorTracker textBasedFloorTracker = (TextBasedFloorTracker) cm.lookup(PROP_FLOOR_MANAGER);
//		final List<PushBuffer> hypListeners = ps.getComponentList(PROP_HYP_CHANGE_LISTENERS, PushBuffer.class);
//		IUDocument iuDocument = new IUDocument();
//		iuDocument.setListeners(hypListeners);
//		SimpleText.createAndShowGUI(hypListeners, textBasedFloorTracker);
		
		// set up the new properties for this class here in the constructor
		PropertySheet gps = cm.getPropertySheet("gruber");
		inlu = (INLUModule) gps.getComponent(INLU_MODULE);
		tree = (TreeModule) gps.getComponent(TREE_MODULE);
		tree.setSession(this.getSession());
		tree.setSessionID(this.getID());
		tree.initDisplay(true, false);
		
		iulisteners = gps.getComponentList(PROP_HYP_CHANGE_LISTENERS, PushBuffer.class);

		
//		for Google ASR
		
//		webSpeech = (GoogleASR) cm.lookup("googleASR");
//		RecoCommandLineParser rclp = new RecoCommandLineParser(new String[] {"-A", "-G", apiKey, "-Lp", "/home/ckennington/Desktop/out"});
//		RecoCommandLineParser rclp = new RecoCommandLineParser(new String[] {"-A" ,"-Lp", "/home/ckennington/Desktop/out"});
//		RecoCommandLineParser rclp = new RecoCommandLineParser(new String[] {"-G", apiKey, "-F", "file:///home/ckennington/git/gruber/Code/diatree/temp.wav", "-Lp", "/home/ckennington/Desktop/out"});
//		RecoCommandLineParser rclp = new RecoCommandLineParser(new String[] {"-G", apiKey, "-M", "-Lp", "/home/ckennington/Desktop/out"});
//		startGoogleASR(cm, rclp);
		
	}
	
	private void prepareFiles() throws IOException {
		String path = "/tmp/gruber//" + getID() + "/"; //problem with Windows machines, /tmp needed for Heroku
		try{ 
			new File(path).mkdirs();
			
		}
		catch (Exception e) {
//			try windows, but use relative
			e.printStackTrace();
			path = "tmp\\gruber\\" + getID() + "\\"; 
			new File(path).mkdirs();
		}
		String sourcePath = "domains/boise/";
		FileUtils.copyFile(new File(sourcePath + "boise.db"), new File(path + "/" + getID() + ".db"));
		FileUtils.copyFile(new File(sourcePath + "boise.txt"), new File(path + "/" + getID() + ".txt"));
		FileUtils.copyFile(new File(sourcePath + "boiseModel.txt"), new File(path + "/" + getID() + "Model.txt"));
	}

	public GruberInstance() {}
	
	@Override
	public void newProperties(PropertySheet ps) {
		super.newProperties(ps);
	}
	
	
	long getTimestamp() {
		return  System.currentTimeMillis();
	}
	
	protected long getTotalElapsedTime() {
		return getTimestamp() - initialTime;
	}

	private void updateWordHyp(String message) {
		int currentFrame = (int) (getTotalElapsedTime() * TimeUtil.MILLISECOND_TO_FRAME_FACTOR); 
		List<WordIU> wordIUs = transcriptToWordList(message);
		List<EditMessage<WordIU>> diffs = chunkHyps.diffByPayload(wordIUs);
		System.out.println(diffs);
		chunkHyps.clear();
		chunkHyps.addAll(wordIUs);
		List<WordIU> ius = new ArrayList<WordIU>();
		for (EditMessage<WordIU> edit: diffs) ius.add(edit.getIU()); 
		// The diffs represents what edits it takes to get from prevList to list, send that to the right buffer
		for (PushBuffer listener : iulisteners) {
			if (listener == null) continue;
			if (listener instanceof FrameAware)
				((FrameAware) listener).setCurrentFrame(currentFrame);
			// update frame count in frame-aware pushbuffers
			if (!diffs.isEmpty())
				listener.hypChange(ius, diffs);
		}
		notifyListeners();
	}
	
	private List<WordIU> transcriptToWordList(String transcript) {
		List<WordIU> words = new ArrayList<WordIU>();
		for (String token : Arrays.asList(transcript.toLowerCase().trim().split("\\s+"))) {
			words.add(new WordIU(token, null, null));
		}
		return words;
	}

	public void reset() {
		tree.reset();
		tree.initDisplay(true, true);
		tree.logReset();
		chunkHyps = new IUList<WordIU>();
	}
	
	public void updateMessage(JSONObject message) {
		InteractionLogger.logger.info(getID() + " messageFromClient=" + message.toString());
		try {
			if (message.has("transc")) {
				updateWordHyp(message.getString("transc"));	
			}
			else if (message.has("initConcepts")) {
				updateInitConcepts(message);
			}
			else {
				JSONObject json = message;
				if (json.has("words")) {
					trainCount++;
					String words = json.getString("words").trim();
					Iterator<?> keys = json.keys();
			    	while (keys.hasNext()) {
			    		String key = (String)keys.next();
			    		if ("target".equals(key)) continue;
			    		if (key.equals("words")) continue;
			    		String value = json.getString(key).replaceAll("_", " ");
		//	    		List<String> values = Arrays.asList(json.getString(key).split("_"));
		//	    		for (String value : values) {
			    			System.out.println("value: " + value);
			    			inlu.model.addExampleForConcept(value, words);
		//	    		}
			    	}
			    	
			    	if (trainCount > 2) inlu.model.train();
		    	
				}
				reset();
			}
//	    	
		} 
		catch (JSONException e) {
			e.printStackTrace();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void updateInitConcepts(JSONObject message) throws JSONException {
		JSONArray json = message.getJSONArray("initConcepts");
		LinkedList<String> intents = new LinkedList<String>();
		LinkedList<String> concepts = new LinkedList<String>();
		List<String> possibleIntents = inlu.model.getPossibleIntents();
		for (int j=0; j<json.length(); j++) {
			JSONObject obj = new JSONObject(json.get(j).toString());
			String name = obj.getString("name");
			JSONArray types = obj.getJSONArray("types");
			for (int i=0; i<types.length(); i++){
				String typeName  = (String) types.get(i);
				if (possibleIntents.contains(typeName)) {
						intents.add(typeName);
						concepts.add(name);
				}
			}
		}	
		try {
			inlu.model.addConceptForIntent(intents, concepts);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius, List<? extends EditMessage<? extends IU>> edits) {

		
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public int getVID() {
		return VID;
	}

	public void setVID(int vID) {
		VID = vID;
	}
	

}
