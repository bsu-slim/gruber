package util;

import org.json.simple.parser.JSONParser;
import org.json.JSONException;
import org.json.JSONObject;

import model.Constants;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * JSON manipulation functions.
 */
public class JSONUtils {

	public static String getIDMessageJSON(int id) throws JSONException {
			JSONObject rootJSON = new JSONObject();
			rootJSON.put("id", id);
			return rootJSON.toString();
	}
	
	public static String getRoleMessageJSON(String role) throws JSONException {
		JSONObject rootJSON = new JSONObject();
		rootJSON.put("role", role);
		return rootJSON.toString();
	}
	
	public static String formatMessage(String message) throws JSONException {
		JSONObject rootJSON = new JSONObject();
		rootJSON.put("message", message);
		return rootJSON.toString();
	}
	

	public static JSONObject loadProperties() {
		JSONParser parser = new JSONParser();
		try{
			String line, jsonData = "";
			BufferedReader br = new BufferedReader(new FileReader("properties.json"));
			while ((line = br.readLine()) != null)
			{
				jsonData += line + "\n";
			}
			JSONObject constants = new JSONObject(jsonData);
			return constants;
		}
		catch(Exception e){
			System.err.println("Cannot find properties file.");
			e.printStackTrace();
			System.exit(1);
		}

		return null;
	}

	public static String getVIDMessageJSON(int vid) throws JSONException {
		JSONObject rootJSON = new JSONObject();
		rootJSON.put("vid", vid);
		return rootJSON.toString();
	}
	
	

}
