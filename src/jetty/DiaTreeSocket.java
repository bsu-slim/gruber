package jetty;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.json.JSONException;
import org.json.JSONObject;

import edu.cmu.sphinx.util.props.Configurable;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Component;
import inpro.incremental.FrameAware;
import inpro.incremental.IUModule;
import inpro.incremental.PushBuffer;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.IUList;
import inpro.incremental.unit.WordIU;
import inpro.util.TimeUtil;
import log.InteractionLogger;
import model.GruberInstance;
import module.INLUModule;
import module.TreeModule;
import util.JSONUtils;


@WebSocket
public class DiaTreeSocket{
	
	
//	@S4Component(type = AudioStreamQueue.class)
//	public final static String AUDIO_STREAM_QUEUE = "audioStreamQueue";
//	private AudioStreamQueue queue;
	HashMap<Integer,GruberInstance> sessions = new HashMap<Integer,GruberInstance>();
	Random random = new Random();
	int min = 1;
	int max = 3;
	
	
	public int getNextID() {
		return Math.abs(random.nextInt());
	}
	
	public int getVariantID() {
		return random.nextInt(max - min + 1) + min;
	}
	
	public void sendVID(int id, int vid) {
        try {
            sessions.get(id).getSession().getRemote().sendString(JSONUtils.getVIDMessageJSON(vid));
        } 
        catch (IOException e) {
            e.printStackTrace();
        } 
        catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void sendID(int id) {
        try {
            sessions.get(id).getSession().getRemote().sendString(JSONUtils.getIDMessageJSON(id));
        } 
        catch (IOException e) {
            e.printStackTrace();
        } 
        catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        System.out.println("Close: statusCode=" + statusCode + ", reason=" + reason);
    }

    @OnWebSocketError
    public void onError(Throwable t) {
        System.out.println("Error: " + t.getMessage());
    }

    @OnWebSocketConnect
    public void onConnect(Session s) {
    	try {
    		int id = getNextID();
    		int vid = getVariantID();
			sessions.put(id, new GruberInstance(s, id, vid));
			sendID(id);
			sendVID(id, vid);
		} catch (PropertyException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
    }
    
	@OnWebSocketMessage
    public void onMessage(byte[] data, int offset, int length) {
//    	queue.addToQueue(data, offset, length);
    }

    @OnWebSocketMessage
    public void onMessage(String message) {
    	System.out.println("MESSAGE:   " + message);
    	JSONObject json; try {
			json = new JSONObject(message);
			if (!json.has("id")) return;
			int id  = json.getInt("id");
			sessions.get(id).updateMessage(json.getJSONObject("msg"));
		} 
    	catch (JSONException e) {
			e.printStackTrace();
		}
    }
    
	
//    
//    public static void main(String[] args) {
//    	
//    	DiaTreeSocket d = new DiaTreeSocket();
//    	
//    	for (int i=1; i<100; i++) {
//    		System.out.println(d.getNextID());
//    	}
//    }
//


//	public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response)
//			throws IOException, ServletException {
//		 System.out.println("Target: " + target);
//	}
	
}
