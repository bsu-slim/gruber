package log;

import org.slf4j.LoggerFactory;

import model.Constants;

import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.logging.*;

public class InteractionLogger {
	
	public static Logger logger;	// Singleton logger for use by program
	
		/**
		 * Read through the properties file to get data and store the result
		 * (minimizing IO operations).
		 */
		public static void initConstants(Boolean isSim) {
			// set a system property such that Simple Logger will include timestamp
			System.setProperty("org.slf4j.simpleLogger.showDateTime", "true");
			// set a system property such that Simple Logger will include timestamp in the given format
			System.setProperty("org.slf4j.simpleLogger.dateTimeFormat", "dd-MM-yy HH:mm:ss:SSS");
			// configure SLF4J Simple Logger to redirect output to a file
			if (isSim)
				System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "warn");
			
//			logger = LoggerFactory.getLogger(Constants.class);
			
			//This statement is used to log any messages.
			logger = LoggerFactory.getLogger(InteractionLogger.class);
//			//I am not sure if this method is defined for slf4j logger.
//			logger.setlevel(Level.ALL);
		}
		
		
		
				
		public static void log(String message) {
			
		}

}
